<?php
/**
 * Created by PhpStorm.
 * User: iamkree
 * Date: 09.04.18
 * Time: 12:21
 */

namespace App\Http\Controllers;

use HTML24\MBTilesGenerator\MBTilesGenerator;
use HTML24\MBTilesGenerator\TileSources\MBTilesTileSource;
use HTML24\MBTilesGenerator\Model\BoundingBox;
use ZipArchive;

class MbtilesController extends Controller
{

    private $mapVersion = 'world_v3_z0-13.mbtiles';
    private $prjLocation = '/windy_mbtiles_server';
    private $maxZoom = 12;

    protected function checkCRC($crc, $params) {

        $secretCode = $this->mapVersion;
        foreach ($params as $k => $v) {
            $secretCode .= $v;
        }
        if( $crc == md5($secretCode)) {
            return true;
        }
        return false;
    }

    public function getMbtilesVersion() {
        return response()->json(['request' => [],
            'status' => 200,
            'message' => '',
            'response' => ['map-version' => $this->mapVersion]], 200);
    }

    public function getBoundingBox($crc, $bbox, $maxZoom)
    {
        $milliseconds = round(microtime(true) * 1000);

        $contents = storage_path();
        $scriptStore = $contents . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR;

        try {
            //if( !$this->checkCRC($crc, ['bbox' => $bbox, 'maxzoom' => $maxZoom]) ) {
            //    throw new \Exception("Incorrect crc key");
            //}
            $res = $this->validateInput($bbox, $maxZoom);
            if( !is_array($res) && !isset($res['bbox']) ) {
                throw new \Exception($res);
            }

            $maxZoom = $res['maxzoom'];
            $bbox = $res['bbox'];
            $filename = md5($bbox) . '_z' . (int)$maxZoom . '.mbtiles';
            $mbtilesSet = $scriptStore . $filename;

            $manifest = $this->getManifestData($mbtilesSet . '.json');
            if( $manifest !== false && file_exists($mbtilesSet . '.zip') ) {
                return response()->json(['request' => ['bbox' => $bbox, 'maxzoom' => $maxZoom],
                    'status' => 200,
                    'message' => '',
                    'response' => $manifest], 200);
            }


            $mapVersion = $this->mapVersion;
            $tile_source = new MBTilesTileSource($this->prjLocation . DIRECTORY_SEPARATOR . $mapVersion);
            //$tile_source->setAttribution('Data, imagery and map information provided by MapQuest, OpenStreetMap <http://www.openstreetmap.org/copyright> and contributors, ODbL <http://wiki.openstreetmap.org/wiki/Legal_FAQ#I_would_like_to_use_OpenStreetMap_maps._How_should_I_credit_you.#>.');
            $tile_generator = new MBTilesGenerator($tile_source);
            $bounding_box = new BoundingBox($bbox);
            $tile_generator->setMaxZoom((int)$maxZoom);

            $mbtilesSetFilesize = 0;
            if( !file_exists($mbtilesSet . '.zip') ) {
                $tile_generator->generate($bounding_box, $mbtilesSet);
                $mbtilesSetFilesize = filesize($mbtilesSet);
            }
            $zipArchive = $this->zipFile($mbtilesSet, $filename);
            $zipFilesize = filesize($zipArchive);

            $response = ['mbtiles-set' => DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . $filename . '.zip',
                'filesize-bytes' => $mbtilesSetFilesize,
                'zip-filesize-bytes' => $zipFilesize,
                'map-version' => $mapVersion];

            $this->createManifest($mbtilesSet . ".json", $response);

            if( file_exists($mbtilesSet) ) {
                $this->dropFile($mbtilesSet);
            }

        } catch(\Exception $ex) {
            return response()->json(['request' => ['bbox' => $bbox, 'maxzoom' => $maxZoom],
                'status' => 500,
                'message' => $ex->getMessage(),
                'response' => []], 200);
        }
        $milliseconds_end = round(microtime(true) * 1000);
        $res = $milliseconds_end - $milliseconds;
        $response['time'] = $res;


        return response()->json(['request' => ['bbox' => $bbox, 'maxzoom' => $maxZoom],
            'status' => 200,
            'message' => '',
            'response' => $response], 200);
    }

    protected function zipFile($file, $localname) {
        $zip = new ZipArchive;
        if( file_exists($file . '.zip') ) {
            return $file . '.zip';
        }
        if ($zip->open($file . '.zip', ZipArchive::CREATE) === true) {
            $zip->addFile($file, $localname);
            $zip->close();
            return $file . '.zip';
        } else {
            return false;
        }

    }

    protected function dropFile($file) {
        unlink($file);
    }

    protected function createManifest($file, array $data) {
        $json = json_encode($data);
        file_put_contents($file, $json);
    }

    protected function getManifestData($file) {
        if(!file_exists($file)) {
            return false;
        }
        $json = file_get_contents($file);
        return json_decode($json);
    }

    protected function validateInput($bbox, $maxZoom) {
        if((int)$maxZoom == 0 || (int)$maxZoom > $this->maxZoom) {
            $maxZoom = $this->maxZoom;
        }
        $bboxArr = explode(',', $bbox);
        if(count($bboxArr) != 4) {
            return "Wrong input parameter - bbox. It should contain 4 float values delimited with comma";
        }

        $bboxModified = [];
        for($i = 0; $i < count($bboxArr); $i++) {
            if($i%2 == 0 && ((float)$bboxArr[$i] < -180 || (float)$bboxArr[$i] > 180)) {
                return "Wrong longitude value";
            }
            if($i%2 != 0 && ((float)$bboxArr[$i] < -90 || (float)$bboxArr[$i] > 90)) {
                return "Wrong latitude value";
            }
            $bboxModified[] = (float)$bboxArr[$i];
        }
        $bbox = implode(',', $bboxModified);

        return ['bbox' => $bbox, 'maxzoom' => $maxZoom];
    }
}
