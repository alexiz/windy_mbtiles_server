<?php
/**
 * Created by HTML24 ApS.
 */

namespace HTML24\MBTilesGenerator\TileSources;


use HTML24\MBTilesGenerator\Exception\TileNotAvailableException;
use HTML24\MBTilesGenerator\Model\Tile;

class MBTilesTileSource implements TileSourceInterface
{

    /**
     * @var \PDO
     */
    protected $db;

    /**
     * @var \PDOStatement
     */
    protected $tile_fetcher;

    protected $map_fetcher;
    protected $images_fetcher;

    /**
     * @param $source_file
     * @throws \Exception
     */
    public function __construct($source_file)
    {
        if (!file_exists($source_file)) {
            throw new \Exception('Source not found');
        }

        $this->db = new \PDO('sqlite:' . $source_file);

        $this->validateTable();

        $this->tile_fetcher = $this->db->prepare(
            'SELECT tile_data FROM tiles WHERE tile_column = :x AND tile_row = :y LIMIT 1'
        );

        $this->map_fetcher = $this->db->prepare(
            'SELECT tile_id FROM map WHERE tile_column = :x AND tile_row = :y AND zoom_level = :z LIMIT 1'
        );

        $this->images_fetcher = $this->db->prepare(
            'SELECT tile_data FROM images WHERE tile_id in (:tid) LIMIT 1'
        );
    }

    /**
     * @throws \Exception
     */
    protected function validateTable()
    {
        // Check if this looks like a mbtiles database

        $tables = $this->db->query(
            'SELECT tbl_name FROM sqlite_master WHERE type = \'table\' OR type = \'view\''
        )->fetchAll(\PDO::FETCH_COLUMN, 0);

        if (!in_array('metadata', $tables) || !in_array('map', $tables) || !in_array('images', $tables)) {
            throw new \Exception('This does not look like a valid mbtiles file');
        }

        // Check for the necessary columns
        $metadata = $this->db->query('PRAGMA table_info(metadata)')->fetchAll(\PDO::FETCH_COLUMN, 1);
        $map = $this->db->query('PRAGMA table_info(map)')->fetchAll(\PDO::FETCH_COLUMN, 1);
        $images = $this->db->query('PRAGMA table_info(images)')->fetchAll(\PDO::FETCH_COLUMN, 1);

        if (
            !in_array('name', $metadata) ||
            !in_array('value', $metadata)
        ) {
            throw new \Exception('Metadata table is of incorrect format');
        }

        if (
            !in_array('zoom_level', $map) ||
            !in_array('tile_column', $map) ||
            !in_array('tile_row', $map) ||
            !in_array('tile_id', $map)
        ) {
            throw new \Exception('Map table is of incorrect format');
        }

        if (
            !in_array('tile_data', $images) ||
            !in_array('tile_id', $images)
        ) {
            throw new \Exception('Images table is of incorrect format');
        }
    }

    /**
     * This method will be called before actually requesting single tiles.
     *
     * Use this to batch generate/download tiles.
     * @param Tile[] $tiles
     * @return void
     */
    public function cache($tiles)
    {
        // This source does not need to cache anything
    }

    public function getMap(Tile $tile) {
        $this->map_fetcher->bindValue(':x', $tile->x);
        $this->map_fetcher->bindValue(':y', $tile->y);
        $this->map_fetcher->bindValue(':z', $tile->z);

        $this->map_fetcher->execute();

        $tile_id = $this->map_fetcher->fetchColumn();
        if ($tile_id === false) {
            throw new TileNotAvailableException();
        }

        return $tile_id;
    }

    public function getImage($tile_id) {
        $this->images_fetcher->bindValue(':tid', $tile_id);

        $this->images_fetcher->execute();

        $tile_data = $this->images_fetcher->fetchColumn();
        if ($tile_data === false) {
            throw new TileNotAvailableException();
        }

        return $tile_data;
    }

    /**
     * For every tile needed, this function will be called
     *
     * Return the blob of this image
     * @param Tile $tile
     * @throws TileNotAvailableException
     * @return string Blob of this image
     */
    public function getTile(Tile $tile)
    {
        $this->tile_fetcher->bindValue(':x', $tile->x);
        $this->tile_fetcher->bindValue(':y', $tile->y);

        $this->tile_fetcher->execute();

        $tile_data = $this->tile_fetcher->fetchColumn();
        if ($tile_data === false) {
            throw new TileNotAvailableException();
        }

        return $tile_data;
    }

    /**
     * Return the attribution text as HTML/text
     *
     * @return string
     */
    public function getAttribution()
    {
        $attribution = $this->db->query('SELECT value FROM metadata WHERE name = \'attribution\'');
        if ($attribution->columnCount() > 0) {
            return $attribution->fetchColumn();
        } else {
            return '';
        }
    }

    /**
     * Should return the format of the tiles, either 'jpg' or 'png'
     *
     * @return string
     */
    public function getFormat()
    {
        $format = $this->db->query('SELECT value FROM metadata WHERE name = \'format\'');
        if ($format->columnCount() > 0) {
            return $format->fetchColumn();
        } else {
            return 'jpg';
        }
    }

}
