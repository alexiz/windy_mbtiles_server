<?php
/**
 * Created by HTML24 ApS.
 */

namespace HTML24\MBTilesGenerator\Core;


class MBTileFile
{
    /**
     * @var \PDO
     */
    protected $db;

    /**
     * @var \PDOStatement
     */
    protected $addMetaStmt;

    /**
     * @var \PDOStatement
     */
    protected $addTileStmt;

    protected $addImageStmt;
    protected $addMapStmt;

    /**
     * @param string $file
     */
    public function __construct($file)
    {
        $this->db = new \PDO('sqlite:' . $file);

        $this->db->exec('CREATE TABLE metadata (name TEXT, value TEXT);');
        $this->db->exec('CREATE TABLE map (
                       zoom_level INTEGER,
                       tile_column INTEGER,
                       tile_row INTEGER,
                       tile_id TEXT,
                       grid_id TEXT
                    );'
        );
        $this->db->exec('CREATE INDEX map_grid_id ON map (grid_id);');
        $this->db->exec('CREATE UNIQUE INDEX map_index ON map (zoom_level, tile_column, tile_row);');

        $this->db->exec('CREATE TABLE images (
                    tile_data blob,
                    tile_id text
                  );'
        );

        $this->db->exec('CREATE UNIQUE INDEX images_id ON images (tile_id);');

        $this->db->exec('CREATE VIEW tiles AS
                    SELECT
                        map.zoom_level AS zoom_level,
                        map.tile_column AS tile_column,
                        map.tile_row AS tile_row,
                        images.tile_data AS tile_data
                    FROM map
                    JOIN images ON images.tile_id = map.tile_id;');

        $this->db->beginTransaction();

        $this->addMetaStmt = $this->db->prepare('INSERT INTO metadata (name, value) VALUES (:name, :value)');
        $this->addImageStmt = $this->db->prepare(
            'INSERT INTO images (tile_id, tile_data) VALUES (:tile_id, :tile_data)'
        );
        $this->addMapStmt = $this->db->prepare(
            'INSERT INTO map (zoom_level, tile_column, tile_row, tile_id) VALUES (:z, :x, :y, :tile_id)'
        );
    }


    public function __destruct()
    {
        $this->db->commit();
    }

    /**
     * @param string $name
     * @param string $value
     */
    public function addMeta($name, $value)
    {
        $this->addMetaStmt->bindValue(':name', $name, \PDO::PARAM_STR);
        $this->addMetaStmt->bindValue(':value', $value, \PDO::PARAM_STR);
        $this->addMetaStmt->execute();
    }

    public function addImage($tile_id, $tile_data) {
        $this->addImageStmt->bindValue(':tile_id', $tile_id, \PDO::PARAM_STR);
        $this->addImageStmt->bindValue(':tile_data', $tile_data, \PDO::PARAM_LOB);
        $this->addImageStmt->execute();
    }

    public function addMapItem($zoom_level, $x, $y, $tile_id) {
        $this->addMapStmt->bindValue(':z', $zoom_level, \PDO::PARAM_INT);
        $this->addMapStmt->bindValue(':x', $x, \PDO::PARAM_INT);
        $this->addMapStmt->bindValue(':y', $y, \PDO::PARAM_INT);
        $this->addMapStmt->bindValue(':tile_id', $tile_id, \PDO::PARAM_STR);
        $this->addMapStmt->execute();
    }

    public function insertTilesToMap($iputData) {
        $input = implode('), (', $iputData);
        $stmt = $this->db->prepare(
            'INSERT INTO map (zoom_level, tile_column, tile_row, tile_id) VALUES (' . $input . ')'
        );

        $stmt->execute();
    }
}